# /etc/nixos/configuration.nix

# TODO
#   picom opacity
#   doom emacs , lunarvim
#   tmux work-session

{ config, pkgs, ... }:
let
  user = "hans";
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Swap
  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  # Enable networking
  networking = {
    hostName = "nixos";         # Define your hostname.
    wireless.enable = false;    # Enables wireless support via wpa_supplicant.
    firewall.enable = false;
    networkmanager.enable = true;
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";


  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "nl_NL.UTF-8";
    LC_IDENTIFICATION = "nl_NL.UTF-8";
    LC_MEASUREMENT = "nl_NL.UTF-8";
    LC_MONETARY = "nl_NL.UTF-8";
    LC_NAME = "nl_NL.UTF-8";
    LC_NUMERIC = "nl_NL.UTF-8";
    LC_PAPER = "nl_NL.UTF-8";
    LC_TELEPHONE = "nl_NL.UTF-8";
    LC_TIME = "nl_NL.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    xkbVariant = "";
    libinput.enable = false;   # laptop trackpad
    displayManager = {
       sddm.enable = true;
       autoLogin = {
         enable = true;
         user = "${user}";
       };
     };
     desktopManager.plasma5.enable = true;
  };

  # Font selection
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    noto-fonts-cjk
    hack-font
    jetbrains-mono
    font-awesome
  ];

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = with pkgs; [hplip ];
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${user} = {
    isNormalUser = true;
    description = "hans";
    extraGroups = [ "networkmanager" "wheel" "lp" "scanner" "audio" "video" ];
    shell = pkgs.zsh;
  };
  programs.zsh.enable = true;

  # Omit sudo password
  security.sudo.wheelNeedsPassword = false;

  # Allow unfree packages
  nixpkgs.config = {
    allowUnfree = true;
    alowUnfreePredicate = _: true;
  };

  # Packages installed in system profile. 
  environment = {
    systemPackages = with pkgs; [
      vim_configurable
      wget
      exa
      git
      gnupg
      networkmanager
      pass
      stow
      firefox
      xclip
      #sublime4
      nixos-icons
    ];
    interactiveShellInit = ''
      alias sb='sudo nixos-rebuild switch'
      alias sc='sudo vim /etc/nixos/configuration.nix'
      alias sv='sudo vim'
      alias sr='sudo reboot'
      alias ls='exa -al --group-directories-first'
    '';
  };

  # Auto Channel upgrade
  system.autoUpgrade = {
    enable = true;
    channel = "https://nixos.org/channel/nixos-unstable";
  };

  # Store maintenance
  # manually: [sudo] nix-collect-garbage -d
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      warn-dirty = false;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-then 7d";
    };
  };

  # Service to be enabled
  services.openssh.enable  = true;

  # Generated from system version
  system.stateVersion = "23.05";
}
