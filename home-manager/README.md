# README

## Setup Home-manager

   nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
   nix-channel --update
   sudo nixos-rebuild switch --upgrade

This will create ~/.config/home-manager/home.nix and have to be edited. Also add to /etc/nixos/configuration.nix the line "<home-manager/nixos>" after "./hardware-configuration.nix".

   home-manager switch

```
## Intersting Sites
github.com/yrashk/nix-home/home.nix
github.com/jonathanReeve/dotfiles/dotfiles/home.nix
github.com/bobvanderlinden/nix-home/home.nix
github.com/pinpox/nixos-home/ 
github.com/hbjydev/starnix/ 
github.com/srid/nixos-config
breuer.dev/blog/nixos-home-manager-neovim
hugoreeves.com/posts/2019/nix-home
lucascambiaghi.com/nixpkgs/readme.html
```
