# ~/.config/nixpkgs/home.nix

{ config, pkgs, ... }:

{ 
  programs.home-manager.enable = true;

  imports = [
    #./machine/z13.nix
  ];

  home = {
    username      = "hans";
    homeDirectory = "/home/hans";
    stateVersion  = "23.05";
    sessionVariables = {
      EDITOR   = "vim";
      VISUAL   = "vim";
      BROWSER  = "firefox";
      TERMINAL = "alacrritty";
    };
    packages = with pkgs; [
      exa lf glow bat starship zoxide tealdeer ripgrep ripgrep-all mdcat jq
      tree fd procs unzip zip cron tmux killall fzf btop xclip xsel xsv
      brave kitty alacritty meld imagemagick bottom  tig tree
    ];
  };

  # Enable simple programs
  programs = {
    exa.enable = true;
    zoxide.enable = true;
    starship.enable = true;
    fzf.enable = true;
    fzf.enableZshIntegration = true;
    bat.enable = true;
  };

  programs.zsh = {
    enable = true;
    enableSyntaxHighlighting = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
    shellAliases = {
      sb = "sudo nixos-rebuild switch";
      sc = "sudo vim /etc/nixos/configuration.nix";
      se = "vim ~/.config/home-manager/home.nix";
      sv = "sudo vim";
      sw = "home-manager switch";
      sr = "sudo reboot";
      v  = "vim";
      cl = "clear";
      pg = "ps auxf | grep";
      em = "emacsclient -c -a emacs";
      ls = "exa -al --group-directories-first";
      ga = "git add";
      gb = "git branch";
      gc = "git checkout";
      gcl= "git clone";
      gm = "git commit -m ";
      gp = "git push";
      gpu= "git pull";
      gs = "git status";
      gl = "git log --graph --date-order --date=short --pretty=format:'%C(cyan)%h %C(blue)%ar%C(auto)%d %C(yellow)%s%+b %C(black)%ae'";
    };
    initExtra = ''
      path=("$HOME/.cargo/bin" $path)
      path+=("$HOME/.local/bin")

      eval "$(starship init zsh)"
      eval "$(zoxide init zsh)"
    '';
  };

  # see JonathanReeves
  programs.fish = {
    enable = false;
    interactiveShellInit = ''
      set fish_greeting
      fish_vi_key_bindings
    '';
    shellAbbrs = {
      "sc" = "sudo nvim /etc/nixos/configuration.nix";
      "sb" = "sudo nixos-rebuild switch";
      "se" = "nvim ~/.config/home-manager/home.nix";
      "sw" = "home-manager switch";
      "sr" = "sudo reboot";
      "em" = "emacsclient -c -a emacs";
      "sv" = "sudo nvim";
      "ls" = "exa -al --group-directories-first";
   };
  };

  programs.alacritty = {
    enable = true;
    settings = {
      windows.opacity = 0.85;
      live_config_reload = true;
      font = {
        normal.family = "Hack";
        #bold.family = "hack-font";
        #italic.family = "hack-font";
        size = 11;
      };  
    };
  };

  # https://sw.kovidgoyal.net/kitty [issues/371]
  programs.kitty = {
    enable = true;
    #theme = "Tokyo Night";
    font = {
      name = "Hack";
      size = 11;
    };
    keybindings = {
      "kitty_mode+e" = "kitten hints";
    };
    settings = {
      #config.os_window_close = true;
      copy_on_select = "clipboard";
      selection_foreground = "none";
      selection_background = "none";
    };
  };

  programs.vim = {
    enable = true;
    #extraConfig = builtins.readFile .vim/vimrc;
    extraConfig = ''
      noswapfile
    '';
    settings = {
      relativenumber = true;
      number = true;
      tabstop = 2;
    };
    plugins = with pkgs.vimPlugins; [
      vim-commentary
      vim-surround
      vim-nix
      fzf-vim
      rainbow
    ];
  };

  programs.neovim = {
    enable = true;
    vimAlias = false;
    extraConfig = ''
      colorscheme gruvbox
      let g:context_nvim_no_redraw = 1
      set mouse=a
      set number
      set relativenumber
      set termguicolors
      set splitright splitbelow
      set nowrap
      set clipboard=unnamedplus
      set expandtab
      set nobackup
      set undofile
      set undodir=~/.local/share/nvim/undofir
      set incsearch
      set nocompatible
      set noswapfile
      set hidden
    '';
    plugins = with pkgs.vimPlugins; [
      editorconfig-vim
      gruvbox-community
      vim-commentary
      vim-airline
      vim-elixir
      vim-nix
    ]; 
  };

  programs.git = {
    enable = true;
    userEmail = "hans@hahoweb.nl";
    userName = "Hans Hofman";
    signing.key = "GPG-KEY-ID";
    signing.signByDefault = false;
    extraConfig = {
      init.defaultBranch = "main";
    };
    ignores = [
      ".direnv"
      "flake.nix"
      "flake.lock"
      ".envrc"
      ".data"
      "vendor"
	  ]; 
  };

  #github.com/jeaye/nix-files/blob/master/workstation/user/jeaye/data/dotfiles/tmux.conf
  programs.tmux = {
    enable = true;
    clock24 = true;
    #shortcut = "a";
    baseIndex = 1;
    escapeTime = 0;

    plugins = with pkgs; [
      tmuxPlugins.better-mouse-mode
    ];

    extraConfig = ''
      unbind-key C-b
      set-option -g prefix C-a
      bind-key C-a

      set -g default-terminal "xterm-256color"
      set-option -g mouse on
      bind | split-window -h -c "#{pane_current_path}"
      bind - split-window -v -c "#{pane_current_path}"
      bind c new-window -c "#{pame_current_path}"
      
      set-window-option -g mode-keys vi
      bind-key -t vi-copy 'v' begin-selection
      bind-key -t vi-copy 'y' copy-selection
      bind P paste-buffer
    '';
  };

}

